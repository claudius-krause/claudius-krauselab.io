---
layout: page
title: Curriculum Vitae of Claudius G. Krause
permalink: /cv/
---

You can download my CV as pdf [here](/assets/CV.C.Krause.07.2024.pdf).

## Research Interests

I am interested in physics beyond the Standard Model (SM) and how to answer fundamental questions about Nature around us. Especially I am interested in

- Machine Learning Techniques:\\
_How can we use advanced Neural Networks to improve data analysis in Particle Physics?_\\
_How can we understand what the Neural Networks learn, and what can we learn from that?_

- Effective Field Theories and their application to Higgs physics:\\
_How can we analyze the current experimental data as model-independently as possible and at the same time use a format that will still be usefull in 10 years from now?_

- The Phenomenology of the Higgs particle:\\
_Is it really the Higgs particle, as predicted by the Standard Model?_\\
_And if it is not the Standard Model Higgs, what else is it?_\\
_Can we explain the mass hierarchies or matter-antimatter asymmetry with it?_

More details about my research projects can be found [here](/research/). 

<br><br>
I am a member of the [MODE Collaboration](https://mode-collaboration.github.io/). We use differential programming and deep generative models to optimize the design of future particle detectors. 
<br><br>

## Research Experience

#### **Group Leader (tenure-track)**,
October 2023 to present\\
at the [Institut für Hochenergiephysik (HEPHY)](https://www.oeaw.ac.at/hephy/) of the [Austrian Academy of Sciences (ÖAW)](https://www.oeaw.ac.at/), Vienna, Austria\\
Research focus: _Machine Learning for particle physics_


#### **ITP Fellow (Senior Postdoc)**,
October 2022 to September 2023 \\
at the [Institut für Theoretische Physik](https://www.thphys.uni-heidelberg.de/) of the [University of Heidelberg](https://www.uni-heidelberg.de/de), Heidelberg, Germany\\
Research focus: _Machine Learning for particle physics_


#### **Postdoctoral Associate**,
October 2020 to September 2022 \\
in the [Department of Physics and Astronomy](https://physics.rutgers.edu) of [Rutgers University](https://www.rutgers.edu/), Piscataway, USA\\
Research focus: _Machine Learning for particle physics_


#### **Feodor Lynen Research Fellow of the Alexander von Humboldt Foundation**,
March 2018 to September 2020 \\
hosted by Prof. Dr. M. Carena in the [Theory Department](http://theory.fnal.gov/) of [Fermi National Accelerator Laboratory (Fermilab)](http://fnal.gov), Chicago, USA\\
Research focus: _Higgs Phenomenology beyond the Standard Model, Machine Learning_

#### **Postdoctoral Researcher**,
November 2016 to February 2018 \\
in the [LHC Pheno Group](http://ific.uv.es/lhcpheno/) of Prof. Dr. A. Pich at the [Instituto de Física Corpuscular (IFIC)](http://webific.ific.uv.es/web/), Valencia, Spain\\
Research focus: _Effective Field Theories and Higgs Phenomenology_

#### **Scientific Assistant / Doctoral Student**,
September 2013 to October 2016 \\
in the group of Prof. Dr. G. Buchalla at the [Arnold Sommerfeld Center](https://www.theorie.physik.uni-muenchen.de/) of the [Ludwig Maximilian University](https://www.uni-muenchen.de/index.html), Munich, Germany\\
Research focus: _Higgs Effective Field Theories_

<br><br>

## Education

#### **Dr. rer. nat. in Physics**

- September 15, 2016, Ludwig Maximilian University, Munich, Germany
- Doctoral Thesis: [_Higgs Effective Field Theories &mdash; Systematics & Applications_](https://arxiv.org/abs/1610.08537)
- Supervisor: Prof. Dr. G. Buchalla

##### Attended Ph.D. Schools:

- [47. Herbstschule für Hochenergiephysik](http://www.maria-laach.tp.nt.uni-siegen.de/archiv.php?ID=28), September 8-18, 2015, Maria Laach, Germany
- [Physics beyond the Higgs](http://physik.uni-graz.at/schladming2014/), March 1-8, 2014, Schladming, Austria

#### **Master of Science in Physics**

- Summer 2013, Ludwig Maximilian University, Munich, Germany
- Master Thesis: _An effective field theory for electroweak symmetry breaking including a light Higgs_
- Supervisor: Prof. Dr. G. Buchalla

#### **Bachelor of Science in Physics**

- Summer 2010, Brandenburg Technical University, Cottbus, Germany
- Bachelor Thesis: [_The impact of different Monte Carlo models on the cross section measurement of top-pair production at 7-TeV proton-proton collisions_](http://inspirehep.net/record/1113483)
- Supervisor: Prof. Dr. W. Lohmann

<br><br>

## Awards and Fellowships

- 03/2018 to 09/2020, Feodor Lynen Research Fellowship of the Alexander von Humboldt Foundation

- 01/2018, “Universe PhD Award 2017” of the “Cluster of Excellence Universe” in
Munich, category Theory, award for the best theoretical doctoral thesis in
2016/2017

- 09/2013, Award for being among the best 10% of all graduating students in
Physics: Refund of the tuition fees of the LMU Munich

- 04/2012 to 03/2013, Scholarship holder of the Deutschland-Stipendium (Germany-
Scholarship)

- 08/2011 to 02/2012, Erasmus Scholarship for an exchange semester at the EPFL in Lausanne, Switzerland

- 01/2011,  Award for the best Bachelor Thesis of the Faculty 1 of the Brandenburg University of Technology Cottbus in the year 2010

- 02/2009 to 06/2013,  Scholarship holder of the Roland-Berger-Stiftung

- since 07/2006, Member of the German Physical Society. The first year of the membership was an award for the best exam in physics in my school in the Abitur (final school exam).

<br><br>

## Teaching Experience

#### Lecturing

<u>High School level </u>

- 2-day block course with lectures in "Physics for becoming medical students" for the fellows of the Else-Kröner-Fellowship of the Roland-Berger-Foundation, given each in 2021 and 2022. (High school students that plan to study medicine after finishing school.)

<u>Undergraduate level / Bachelor classes</u>

- Quantum mechanics for teachers (1 stand-in lecture for Prof. Dr. G. Buchalla at LMU Munich)

- Theoretical Mechanics (1 stand-in lecture for Prof. Dr. T. Plehn at Heidelberg University)

- Theoretical Thermodynamics (1 stand-in lecture for Prof. Dr. T. Plehn at Heidelberg University)

<u>Graduate level / Master classes</u>

- Quantum electrodynamics (1 stand-in lecture for Prof. Dr. G. Buchalla at LMU Munich)
- Machine Learning for High-Energy Physics (1 stand-in lecture for Prof. Dr. D. Shih at Rutgers University)
- Deep Generative Models for Particle Physics (lecture and tutorial), [3rd Terascale School of Machine Learning](https://indico.desy.de/event/35006/), Doctoral School at DESY Hamburg, October 2022
- Normalizing Flows (90min lecture), [Active Training Course "Advanced Deep Learning"](https://indico.scc.kit.edu/event/2852/), Doctoral School organized by ErUM-Data-Hub, November 2022
- Modern Machine Learning for Particle Physics (4x 90min lectures and tutorials), KSETA doctoral program of Karlsruhe Institute of Technology (KIT), March 2023
- Modern Machine Learning for (Particle) Physics, (8x 90min lectures and tutorials), 50th Heidelberg Graduate Days at the University of Heidelberg, April 2023
- Generative Models at the LHC, (5x 90min lectures and tutorials), MITP School Machine Learning in Particle Theory, July 2023
- Machine Learning in Particle Physics, (3x 45min lectures + tutorials), CRC TRR 257 Summer School "Particle Physics Phenomenology after the Higgs Discovery", October 2024

#### Teaching Assistant

<u>At BTU Cottbus</u>

- Introduction to Physics (2 weeks intensive course as part of the orientation program for new students)

<u>At LMU Munich (8 terms in total)</u>

- Quantum electrodynamics (theoretical lecture for Master students, 1 term) 
- Advanced nuclear and particle physics (experimental lecture for Master students, 1 term)
- QCD and Standard Model (theoretical lecture for Master students, 1 term)
- Atomic and molecular physics (experimental lecture for Bachelor students, 1 term)
- Electrodynamics (theoretical lecture for Bachelor students, 1 term)
- Nuclear and particle physics (experimental lecture for Bachelor students, 1 term)
- Quantum mechanics for teachers (theoretical lecture for Bachelor students, 2 terms)

#### Development of problem sheets

- QCD and Standard Model (theoretical lecture for Master students at LMU Munich, 1 term)

- Analytical Mechanics and Thermodynamics (theoretical lecture for Bachelor students at Heidelberg University, 1 term)\\ Managing 390 students and 21 teaching assistants.


#### Supervising physics lab courses

- physics lab course for physics Bachelor students (1 term at BTU Cottbus)
- physics lab course for engineering Bachelor students (2 terms at BTU Cottbus)

#### Grading of homework assignments

- Mathematik IT-3 (Analysis lecture for Bachelor students at BTU Cottbus, 1 term)

#### Student Co-Supervision

<u>Undergraduate Projects</u>
- Lucas Clement, Rutgers University

<u>Master's Thesis</u>

- Jan-Niklas Toelstede, LMU Munich
- Imahn Shekhzadeh, University Hamburg
- Florian Ernst, Heidelberg University

<u>Doctoral Thesis</u>

- Dr. Joaquín Santos-Blasco, IFIC & Universidad de Valencia
- Dr. Yikun Wang, Fermilab & University of Chicago
- Dr. Anna Hallin, Rutgers University

#### Student Supervision

<u>Undergraduate Projects</u>
- Nina Pellosch, TU Vienna

<br><br>


## Service

- Editorial Fellow of [SciPost Physics](https://scipost.org/), since 04/2024

- Organizer: [Workshop on Machine Learning and High-Energy Physics: "GlühWien"](https://indico.cern.ch/event/1311972/overview), ÖAW Campus Akademie, Vienna, 13-15 December 2023

- Co-Leader of Working Group 2 (Technological innovation in data analysis) of COST action [COMETA](https://www.cost.eu/actions/CA22130/), since 10/2023

- Co-Organizer: [Machine Learning in Particle Theory - MITP Summer School 2023](https://indico.mitp.uni-mainz.de/event/332/), 07/2023

- Moderator of the [HEP-ML Living Review](https://iml-wg.github.io/HEPML-LivingReview/), since 05/2023

- Co-Organizer: [CaloChallenge Workshop](https://agenda.infn.it/event/34036/) in 05/2023

- Co-Organizer: [Fast Calorimeter Simulation Challenge 2022](https://calochallenge.github.io/homepage/)

- Co-Organizer: Particle Phenomenology Seminar at ITP Heidelberg, 10/2022 -- 09/2023

- Co-Organizer: High-Energy Theory Seminar at Rutgers, 01/2021 -- 09/2022

- Organizer: HEP Journal Club at Fermilab, 10/2019 -- 09/2020

- Co-Organizer: [Multibosons At The Energy Frontier](https://indico.cern.ch/event/823181/), Fermilab, 25-26 July 2019 

- I have reviewed a total of more than 20 submissions to scientific journals such as [JHEP](https://jhep.sissa.it/), [ML:ST](https://iopscience.iop.org/journal/2632-2153), [EPJC](https://epjc.epj.org/), [SciPost](https://scipost.org/), [Computing and Software for Big Science](https://link.springer.com/journal/41781), [Nature Communications](https://www.nature.com/ncomms/), and [Phys.Rev.D](https://journals.aps.org/prd/), as well as conferences such as [SIGGRAPH Conference](https://www.siggraph.org/), [ACAT](https://indico.cern.ch/event/1106990/), [ICML 2024 AI for Science Workshop](https://openreview.net/group?id=ICML.cc/2024/Workshop/AI4Science), and [NeurIPS 2024 Workshop ML4PS](https://openreview.net/group?id=NeurIPS.cc/2024/Workshop/ML4PS).


<br><br>

## Outreach

- Presentation "Auf der Jagd nach Dunkler Materie &mdash; mit Koalas und Maschinellem Lernen", Dark Matter Day 2024, 10/2024.

- Presentation about (my) work as a researcher to highschool students, organized by the Roland Berger Foundation, 03/2023.

- Presentation about (my) work as a researcher to highschool students, organized by the Roland Berger Foundation, 10/2020.

- Participating scientist in the monthly Fermilab event "ask-a-scientist", 01/2020.

- Participation in a two day workshop on outreach, organized by the Klaus Tschira Stiftung, as reward for my participation in the "KlarText" competition, 10/2017.

- Participated in the "Expociencia", open house day of IFIC, Valencia, Spain, 05/2017.

- Participated in the "KlarText" competition of the Klaus Tschira Stiftung, writing an outreach essay about my doctoral thesis, 02/2017.


<br><br>

## Publications

##### peer-reviewed

- *"Convolutional L2LFlows: Generating Accurate Showers in Highly Granular Calorimeters Using Convolutional Normalizing Flows"*,\\
By Thorsten Buss, Frank Gaede, Gregor Kasieczka, Claudius Krause, David Shih.\\
[arXiv:2405.20407](https://arxiv.org/abs/2405.20407), [JINST 19 (2024) 09, P09003](https://doi.org/10.1088/1748-0221/19/09/P09003).

- *"Anomaly detection with flow-based fast calorimeter simulators"*,\\
By Claudius Krause, Benjamin Nachman, Ian Pang, David Shih, Yunhao Zhu.\\
[arXiv:2312.11618](https://arxiv.org/abs/2312.11618), [Phys.Rev.D 110 (2024) 3, 035036](https://doi.org/10.1103/PhysRevD.110.035036).

- *"Deep generative models for detector signature simulation: A Taxonomic Review"*,\\
By Baran Hashemi, Claudius Krause.\\
[arXiv:2312.09597](https://arxiv.org/abs/2312.09597), [Reviews in Physics, Volume 12, December 2024, 100092](https://doi.org/10.1016/j.revip.2024.100092).

- *"Combining Resonant and Tail-based Anomaly Detection"*,\\
By Gerrit Bickendorf, Manuel Drees, Gregor Kasieczka, Claudius Krause, David Shih.\\
[arXiv:2309.12918](https://arxiv.org/abs/2309.12918), [Phys. Rev. D 109 (2024) 9, 096031](https://doi.org/10.1103/PhysRevD.109.096031).

- *"CaloFlow for CaloChallenge Dataset 1"*,\\
By Claudius Krause, Ian Pang, David Shih.\\
[arXiv:2210.14245](https://arxiv.org/abs/2210.14245), [SciPost Phys. 16, 126 (2024)](https://doi.org/10.21468/SciPostPhys.16.5.126).

- *"The Interplay of Machine Learning--based Resonant Anomaly Detection Methods"*,\\
By Tobias Golling, Gregor Kasieczka, Claudius Krause, Radha Mastandrea, Benjamin Nachman, John Andrew Raine, Debajyoti Sengupta, David Shih, Manuel Sommerhalder.\\
[arXiv:2307.11157](https://arxiv.org/abs/2307.11157), [Eur. Phys. J. C 84, 241 (2024).](https://doi.org/10.1140/epjc/s10052-024-12607-x).

- *"Inductive simulation of calorimeter showers with normalizing flows"*,\\
By Matthew R. Buckley, Claudius Krause, Ian Pang, David Shih.\\
[arXiv:2305.11934](https://arxiv.org/abs/2305.11934), [Phys. Rev. D 109 (2024) 3, 033006](https://doi.org/10.1103/PhysRevD.109.033006).

- *"How to Understand Limitations of Generative Networks"*,\\
By Ranit Das, Luigi Favaro, Theo Heimel, Claudius Krause, Tilman Plehn, David Shih.\\
[arXiv:2305.16774](https://arxiv.org/abs/2305.16774), [SciPost Phys. 16, 031 (2024)](https://doi.org/10.21468/SciPostPhys.16.1.031).

- *"L2LFlows: Generating High-Fidelity 3D Calorimeter Images"*,\\
By Sascha Diefenbacher, Engin Eren, Frank Gaede, Gregor Kasieczka, Claudius Krause, Imahn Shekhzadeh, David Shih.\\
[arXiv:2302.11594](https://arxiv.org/abs/2302.11594), [JINST 18 (2023) P10017](https://doi.org/10.1088/1748-0221/18/10/P10017).

- *"MadNIS &#8212; Neural Multi-Channel Importance Sampling"*,\\
By Theo Heimel, Ramon Winterhalder, Anja Butter, Joshua Isaacson, Claudius Krause, Fabio Maltoni, Olivier Mattelaer, Tilman Plehn.\\
[arXiv:2212.06172](https://arxiv.org/abs/2212.06172), [SciPost Phys. 15 (2023) 141](https://doi.org/10.21468/SciPostPhys.15.4.141).

- *"CaloFlow II: Even Faster and Still Accurate Generation of Calorimeter Showers with Normalizing Flows"*,\\
By Claudius Krause and David Shih.\\
[arXiv:2110.11377](https://arxiv.org/abs/2110.11377), [Phys. Rev. D 107 (2023), 113004](https://doi.org/10.1103/PhysRevD.107.113004).

- *"CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows"*, \\
By Claudius Krause and David Shih.\\
[arXiv:2106.05285](https://arxiv.org/abs/2106.05285), [Phys. Rev. D 107 (2023), 113003](https://doi.org/10.1103/PhysRevD.107.113003).

- *"Classifying Anomalies THrough Outer Density Estimation (CATHODE)"*, \\
By Anna Hallin, Joshua Isaacson, Gregor Kasieczka, Claudius Krause, Benjamin Nachman, Tobias Quadfasel, Matthias Schlaffer, David Shih, Manuel Sommerhalder. \\
[arXiv:2109.00546](https://arxiv.org/abs/2109.00546), [Phys. Rev. D 106 (2022), 055006](https://doi.org/10.1103/PhysRevD.106.055006).

- *"Higgs-Electroweak Chiral Lagrangian: One-Loop Renormalization Group Equations"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Alejandro Celis, Marc Knecht, Claudius Krause.\\
[arXiv:2004.11348](https://arxiv.org/abs/2004.11348), [Phys. Rev. D 104 (2021) 7, 076005](https://doi.org/10.1103/PhysRevD.104.076005).

- *"A New Approach to Electroweak Symmetry Non-Restoration"*, \\
By Marcela Carena, Claudius Krause, Zhen Liu, Yikun Wang.\\
[arXiv:2104.00638](https://arxiv.org/abs/2104.00638), [Phys.Rev.D 104 (2021) 5, 055016](https://doi.org/10.1103/PhysRevD.104.055016).

- *"i-flow: High-dimensional Integration and Sampling with Normalizing Flows"*,\\
By Christina Gao, Joshua Isaaacson, Claudius Krause.\\
[arXiv:2001.05486](https://arxiv.org/abs/2001.05486), [Mach.Learn.Sci.Tech. 1 (2020) 4, 045023](https://doi.org/10.1088/2632-2153/abab62).

- *"Event Generation with Normalizing Flows"*,\\
By Christina Gao, Stefan Höche, Joshua Isaacson, Claudius Krause, Holger Schulz.\\
[arXiv:2001.10028](https://arxiv.org/abs/2001.10028), [Phys.Rev.D 101 (2020) 7, 076002](https://doi.org/10.1103/PhysRevD.101.076002).

- *"Colorful Imprints of Heavy States in the Electroweak Effective Theory"*,\\
By Claudius Krause, Antonio Pich, Ignasi Rosell, Joaquín Santos, Juan José Sanz-Cillero.\\
[arXiv:1810.10544](https://arxiv.org/abs/1810.10544), [JHEP 1905 (2019) 092](https://doi.org/10.1007/JHEP05(2019)092).

- *"Current and Future Constraints on Higgs Couplings in the Nonlinear Effective Theory"*,\\
By Jorge de Blas, Otto Eberhardt, Claudius Krause.\\
[arXiv:1803.00939](https://arxiv.org/abs/1803.00939), [JHEP 1807 (2018) 048](https://doi.org/10.1007/JHEP07(2018)048).

- *"Signals of the electroweak phase transition at colliders and gravitational wave observatories"*,\\
By Mikael Chala, Claudius Krause, Germano Nardini.\\
[arXiv:1802.02168](https://arxiv.org/abs/1802.02168), [JHEP 1807 (2018) 062](https://doi.org/10.1007/JHEP07(2018)062).

- *"Complete One-Loop Renormalization of the Higgs-Electroweak Chiral Lagrangian"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Alejandro Celis, Marc Knecht, Claudius Krause.\\
[arXiv:1710.06412](https://arxiv.org/abs/1710.06412), [Nucl.Phys. B928 (2018) 93-106](https://doi.org/10.1016/j.nuclphysb.2018.01.009).

- *"Standard Model Extended by a Heavy Singlet: Linear vs. Nonlinear EFT"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Alejandro Celis, Claudius Krause.\\
[arXiv:1608.03564](https://arxiv.org/abs/1608.03564), [Nucl.Phys. B917 (2017) 209-233](https://doi.org/10.1016/j.nuclphysb.2017.02.006).

- *"Fitting Higgs Data with Nonlinear Effective Theory"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Alejandro Celis, Claudius Krause.\\
[arXiv:1511.00988](https://arxiv.org/abs/1511.00988), [Eur.Phys.J. C76 (2016) no.5, 233](https://doi.org/10.1140/epjc/s10052-016-4086-9).

- *"Note on Anomalous Higgs-Boson Couplings in Effective Field Theory"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Alejandro Celis, Claudius Krause.\\
[arXiv:1504.01707](https://arxiv.org/abs/1504.01707), [Phys.Lett. B750 (2015) 298-301](https://doi.org/10.1016/j.physletb.2015.09.027).

- *"A Systematic Approach to the SILH Lagrangian"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Claudius Krause.\\
[arXiv:1412.6356](https://arxiv.org/abs/1412.6356), [Nucl.Phys. B894 (2015) 602-620](https://doi.org/10.1016/j.nuclphysb.2015.03.024).

- *"On the Power Counting in Effective Field Theories"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Claudius Krause.\\
[arXiv:1312.5624](https://arxiv.org/abs/1312.5624), [Phys.Lett. B731 (2014) 80-86](https://doi.org/10.1016/j.physletb.2014.02.015).

- *"Complete Electroweak Chiral Lagrangian with a Light Higgs at NLO"*,\\
By Gerhard Buchalla, Oscar Cat&agrave;, Claudius Krause.\\
[arXiv:1307.5017](https://arxiv.org/abs/1307.5017), [Nucl.Phys. B880 (2014) 552-573](https://doi.org/10.1016/j.nuclphysb.2014.01.018), [Erratum: Nucl.Phys. B913 (2016) 475-478](https://doi.org/10.1016/j.nuclphysb.2016.09.010).

##### preprints

- *"Unifying Simulation and Inference with Normalizing Flows"*,\\
By Haoxing Du, Claudius Krause, Vinicius Mikuni, Benjamin Nachman, Ian Pang, David Shih.\\
[arXiv:2404.18992](https://arxiv.org/abs/2404.18992).

- *"Normalizing Flows for High-Dimensional Detector Simulations"*,\\
By Florian Ernst, Luigi Favaro, Claudius Krause, Tilman Plehn, David Shih.\\
[arXiv:2312.09290](https://arxiv.org/abs/2312.09290).

- *"Master Formula for One-Loop Renormalization of Bosonic SMEFT Operators"*,\\
By Gerhard Buchalla, Alejandro Celis, Claudius Krause, Jan-Niklas Toelstede.\\
[arXiv:1904.07840](https://arxiv.org/abs/1904.07840).

- *"Comment on "Analysis of General Power Counting Rules in Effective Field Theory" "*,\\
By Gerhard Buchalla, Oscar Cata, Alejandro Celis, Claudius Krause.\\
[arXiv:1603.03062](https://arxiv.org/abs/1603.03062).

##### Doctoral Thesis

- *"Higgs Effective Field Theories &mdash; Systematics & Applications"*,\\
By Claudius G. Krause.\\
[arXiv:1610.08537](https://arxiv.org/abs/1610.08537), [University Library](https://edoc.ub.uni-muenchen.de/19873/)

##### community projects

- *"CaloChallenge 2022: A Community Challenge for Fast Calorimeter Simulation"*,\\
By Claudius Krause (main editor), Michele Faucci Giannelli (editor), Gregor Kasieczka (editor), Benjamin Nachman (editor), Dalila Salamani (editor), David Shih , Anna Zaborowska (editor), et al.\\
[arXiv:2410.21611](https://arxiv.org/abs/2410.21611)

- *"Modern Machine Learning for LHC Physicists"*,\\
Lecture Notes by Tilman Plehn, Anja Butter, Barry Dillon, Theo Heimel, Claudius Krause, Ramon Winterhalder.\\
[arXiv:2211.01421](https://arxiv.org/abs/2211.01421)

- *"Snowmass 2021 Computational Frontier CompF03 Topical Group Report: Machine Learning"*,\\
Contribution to Snowmass 2021 (P. Shanahan, K. Terao, D. Whiteson et al.).\\
[arXiv:2209.07559](https://arxiv.org/abs/2209.07559)

- *"Toward the End-to-End Optimization of Particle Physics Instruments with Differentiable Programming: a White Paper"*,\\
The MODE collaboration.\\
[arXiv:2203.13818](https://arxiv.org/abs/2203.13818), [Reviews in Physics 10, June 2023, 100085](https://doi.org/10.1016/j.revip.2023.100085)

- *"Event Generators for High-Energy Physics Experiments"*,\\
Contribution to Snowmass 2021 (J.M. Campbell et al.).\\
[arXiv:2203.11110](https://arxiv.org/abs/2203.11110), [SciPost Phys. 16, 130 (2024)](https://doi.org/10.21468/SciPostPhys.16.5.130).

- *"New directions for surrogate models and differentiable programming for High Energy Physics detector simulation"*,\\
Contribution to Snowmass 2021 (A. Adelmann et al.).\\
[arXiv:2203.08806](https://arxiv.org/abs/2203.08806).

- *"Machine Learning and LHC Event Generation"*,\\
Contribution to Snowmass 2021 (A. Butter et al.).\\
[arXiv:2203.07460](https://arxiv.org/abs/2203.07460), [SciPost Phys. 14, 079 (2023)](https://doi.org/10.21468/SciPostPhys.14.4.079).

- *"Higgs Physics at the HL-LHC and HE-LHC"*,\\
By HL/HE WG2 group (M. Cepeda et al.).\\
[arXiv:1902.00134](https://arxiv.org/abs/1902.00134).

- *"Handbook of LHC Higgs Cross Sections: 4. Deciphering the Nature of the Higgs Sector"*,\\
By LHC Higgs Cross Section Working Group (D. de Florian et al.).\\
[arXiv:1610.07922](https://arxiv.org/abs/1610.07922).

##### conference proceedings

- *"Advancing Generative Modelling of Calorimeter Showers on Three Frontiers"*,\\
By E. Buhmann, S. Diefenbacher, E. Eren, F. Gaede, G. Kasieczka, W. Korcari, A. Korol, C. Krause, K. Krueger, P. McKeown, I. Shekhzadeh, D. Shih.\\
[ML4PS at NeurIPS 2023](https://ml4physicalsciences.github.io/2023/files/NeurIPS_ML4PS_2023_105.pdf), Machine Learning and the Physical Sciences, Workshop at the 37th conference on Neural Information Processing Systems (NeurIPS), December 2023.

- *"Classifying Anomalies THrough Outer Density Estimation (CATHODE)"*,\\
By A. Hallin, J. Isaacson, G. Kasieczka, C. Krause, B. Nachman, T. Quadfasel, M. Schlaffer, D. Shih, M. Sommerhalder.\\
[ML4PS at NeurIPS 2021](https://ml4physicalsciences.github.io/2021/files/NeurIPS_ML4PS_2021_59.pdf), Machine Learning and the Physical Sciences, Workshop at the 35th Conference on Neural Information Processing Systems (NeurIPS), December 2021.

- *"Effective theories and resonances in strongly-coupled electroweak symmetry breaking scenarios"*,\\
By Ignasi Rosell, Claudius Krause, Antonio Pich, Juan José Sanz-Cillero.\\
[arXiv:1910.01839](https://arxiv.org/abs/1910.01839), PoS EPS-HEP2019.

- *"Complete One-Loop Renormalization of the Higgs-Electroweak Chiral Lagrangian"*,\\
By Claudius Krause, Gerhard Buchalla, Oscar Cat&agrave;, Alejandro Celis, Marc Knecht.\\
[arXiv:1907.07605](https://arxiv.org/abs/1907.07605), PoS CD2018 (2018) 072.

- *"Heavy resonances and the electroweak effective theory"*,\\
By Ignasi Rosell, Claudius Krause, Antonio Pich, Joaquín Santos, Juan José Sanz-Cillero.\\
[arXiv:1811.10233](https://arxiv.org/abs/1811.10233), [PoS ICHEP2018 (2019) 316](https://doi.org/10.22323/1.340.0316).

- *"Tracks of resonances in electroweak effective Lagrangians"*,\\
By Ignasi Rosell, Claudius Krause, Antonio Pich, Joaquín Santos, Juan José Sanz-Cillero.\\
[arXiv:1710.06622](https://arxiv.org/abs/1710.06622), [PoS EPS-HEP2017 (2018) 334](https://doi.org/10.22323/1.314.0334).

<br><br>

## Conference Talks and Invited Seminars

- (virtual) Talk at "MODE Collaboration Meeting", November 20, 2024\\
*Final Results of the CaloChallenge 2022*

- Plenary Talk at the ML4Jets Conference, November 6, 2024\\
LPNHE, Sorbonne University, Paris, France\\
[*CaloChallenge 2022 &mdash; Final Evaluation and Lessons Learned*](/assets/Talks/CaloChallenge.FinalEval.C.Krause.pdf)

- Seminar, October 29, 2024\\
University of Geneva, Geneva, Switzerland\\
*Deep Generative Models in Particle Physics*

- Talk at "Physics in the AI era", September 27, 2024\\
University of Pisa, Pisa, Italy\\
*Generative Models in Particle Physics*

- Talk at "Make it (Net)work!" Seminar by MLA2S, September 2, 2024\\
Austrian Academy of Sciences, Vienna, Austria\\
*Machine Learning at the Institute for High-Energy Physics (HEPHY)*

- Invited Seminar, May 29, 2024\\
University of Graz, Graz, Austria\\
*Improving HEP Simulation and Analyses with Invertible Neural Networks*

- Flashtalk (+[poster](/assets/Poster_EuCAIFCon_Flows4Calo.pdf)) at EuCAIFCon 2024, April 30, 2024\\
Amsterdam, The Netherlands\\
*Flow-based generative models for particle calorimeter simulation*

- Talk at the Vienna Workshop on Simulations (VIEWS) 2024, April 27, 2024\\
Vienna, Austria\\
*Deep Generative Models for Calorimeter Simulations*

- Talk at the COMETA WG2 topical meeting on Normalizing Flows in Particle Physics and Beyond, March 28, 2024\\
via zoom\\
*Normalizing Flows for Calorimeter Simulation*

- Introduction at the COMETA WG2 topical meeting on Normalizing Flows in Particle Physics and Beyond, March 28, 2024\\
via zoom\\
*Introduction to Normalizing Flows*

- Summary talk on Working Group 2 Activities of the COMETA COST Action, March 1, 2024\\
Izmir, Türkiye

- Talk at the FAKT Particle Physics Retreat, February 22, 2024\\
Bruck a. d. Mur, Austria\\
*Deep Generative Models in Particle Physics*

- ARI Guest Colloquium, January 17, 2024\\
Acoustic Research Institute (ARI), Institut für Schallforschung (ISF), ÖAW, Vienna, Austria\\
*Machine Learning & Particle Physics*

- Particle Theory Seminar, January 9, 2024\\
University of Vienna, Vienna, Austria\\
*Improving HEP Simulation and Analyses with Invertible Neural Networks*

- Talk at the ML4Jets Conference, November 9, 2023\\
DESY, Hamburg, Germany\\
*The Fast Calorimeter Simulation Challenge 2022* 

- Invited Seminar at the ORIGINS Data Science Lab, August 4, 2023\\
TU Munich, Garching, Germany\\
*Improving HEP Simulation and Analyses with Invertible Neural Networks*

- Remote Talk at the CMS ML Forum, June 28, 2023\\
CERN, Geneva, Switzerland\\
*A First Look at the Fast Calorimeter Challenge 2022*

- Particle Theory Seminar, June 15, 2023\\
University of Würzburg, Germany\\
*Improving HEP Simulation and Analyses with Invertible Neural Networks*

- Physics Colloquium, June 6, 2023\\
Brandenburg Technical University, Cottbus, Germany\\
*How deep generative models change our understanding of Nature*

- Plenary Talk at the "CaloChallenge Workshop", May 31, 2023\\
Villa Mondragone, Frascati, Italy\\
*The Fast Calorimeter Challenge 2022: Results and The Road Ahead*

- Invited Plenary Talk at "PLANCK 2023 &#8212; The 25th International Conference From the Planck Scale to the Electroweak Scale", May 22, 2023\\
Ochata Campus, University of Warsaw, Poland\\
*Modern Machine Learning for Particle Physics*

- Invited Talk at "Prospecting for New Physics through Flavor, Dark Matter, and Machine Learning", March 28, 2023\\
Aspen Center for Physics, Aspen, Colorado, USA\\
*Machine Learning for Event Generation and Fast Simulation*

- Invited Talk at "AI and Quantum Information Applications in Fundamental Physics", February 14, 2023\\
Konjiam, South Korea\\
*Fast and Faithful (Calorimeter) Simulations with Normalizing Flows*

- Invited TTK Theory Seminar at the RWTH Aachen, January 19, 2023\\
RWTH Aachen, Germany\\
*Searching the Unknown: Anomaly Detection at the LHC and with Gaia*

- Talk at "Foundation models and fast detector simulation", November 21, 2022\\
CERN, Geneva, Switzerland\\
*Calorimeter Simulation with Normalizing Flows*

- Invited Talk at the ML4Jets Conference, November 2, 2022\\
Rutgers University, New Jersey, USA\\
*Generative Models for Fast Detector Simulation*

- Remote Phenomenology Seminar at the Oklahoma State University, October 20, 2022\\
Oklahoma State University, Stillwater, Oklahoma, USA\\
*Improving HEP Simulation and Analyses with Invertible Neural Networks*

- Machine Learning in High Energy Physics, September 12, 2022\\
HEPHY Vienna, Austria\\
*Normalizing Flows at the LHC - Preparing for the Future*

- Plenary Talk at the IAIFI Summer Workshop, August 8, 2022\\
Tufts University, Medford, Massachusetts, USA\\
*Normalizing Flows at the LHC*

- Snowmass Community Summer Study Workshop, July 22, 2022\\
University of Washington, Seattle, Washington, USA\\
*Machine Learning for Collider Theory*

- Particle Phenomenology Seminar at the ITP, Heidelberg University, June 23, 2022\\
University of Heidelberg, Germany\\
*Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows: CaloFlow*

- Invited Talk at the "AI goes MAD" workshop, June 15, 2022\\
Instituto de F&iacute;sica Te&oacute;rica, Universidad Aut&oacute;noma de Madrid, Spain\\
*Breaking Simulation Bottlenecks with Normalizing Flows*

- Invited Talk at the "Mitchell Conference on Collider, Dark Matter, and Neutrino Physics", May 25, 2022\\
Mitchell Institute at Texas A&amp;M University, College Station, Texas, USA\\
*Fast and Faithful Generation of Calorimeter Showers with Deep Generative Models: CaloFlow*

- Remote Talk at the 10th Edition of the Large Hadron Collider Physics Conference (LHCP 2022), May 18, 2022\\
online, hosted in Taipeh, Taiwan\\
*New developments in fast simulation with machine learning*

- Remote Talk at the CMS ML Forum on Normalizing Flows, April 20, 2022\\
CERN, Geneva, Switzerland\\
*Application of Normalizing Flows to Calorimeter Shower Generation: CaloFlow*

- Joint virtual meeting of the HSF detector simulation and ML4Sim working groups, April 11, 2022\\
*The Fast Calorimeter Simulation Challenge 2022*

- Remote Theory Seminar at Fermilab, February 10, 2022\\
Fermi National Accelerator Laboratory (Fermilab), Chicago, USA\\
*CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows*

- Remote Talk at the "Machine Learning for Fundamental Physics Group" meeting, December 2, 2021\\
Lawrence Berkeley National Laboratory, California, USA\\
*CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows*

- Remote Talk at the "New ML Techniques for Simulation" meeting of the HEP Software Foundation, Detector Simulation Working Group, November 8, 2021\\
virtual, worldwide\\
*CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows*

- Remote Talk at the "Machine Learning for Simulation" Meeting, October 28, 2021\\
CERN, Geneva, Switzerland\\
*A Classifier as an Ultimate Metric: The CaloFlow Example*

- Remote Talk at the "IML Machine Learning Working Group" Meeting, October 5, 2021\\
CERN, Geneva, Switzerland\\
*Normalizing Flows for Calorimeter Showers: When the Student beats the Teacher*

- (virtual) Seminar for Relayr Munich, September 29, 2021\\
Relayr, Munich, Germany\\
*The ABC of Normalizing Flows: Application, Basics, and Concepts*

- (virtual) Talk at "First MODE Workshop on Differentiable Programming", September 7, 2021\\
Université catholique de Louvain, CP3, Belgium\\
*Generation of Calorimeter Showers with Normalizing Flows: CaloFlow*

- (virtual) Talk at "2021 Meeting of the Division of Particles and Fields of the American Physical Society (DPF21)", July 12, 2021\\
Florida State University, Tallahassee, Florida, USA\\
*CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows*

- (virtual) Talk at "ML4Jets 2021", July 7, 2021\\
University of Heidelberg, Germany\\
*CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows*

- (virtual) Talk at "Machine Learning for Particle Physics", June 23, 2021\\
MITP Virtual Workshop, Mainz, Germany\\
*Generative Modeling with Normalizing Flows: CaloFlow*

- Talk at the Rutgers Physics & Astronomy Machine Learning Group Meeting, February 8, 2021\\
Rutgers University, New Jersey, USA\\
*Normalizing Flows and their Application to Particle Physics*

- Remote Talk at the CMS ML Forum on Event Generation, January 13, 2021\\
CERN, Geneva, Switzerland\\
*i-flow: High-Dimensional Integration and Sampling with Normalizing Flows*

- (virtual) Theory Seminar at the University of Siegen, December 7, 2020\\
University of Siegen, Germany\\
*Improving Event Generation with Machine Learning*

- Remote Talk at IML Machine Learning Working Group Meeting, September 8, 2020\\
CERN, Geneva, Switzerland\\
*Efficient Event Generation with Normalizing Flows*

- (virtual) Theory Seminar at the ITP, Heidelberg University, June 9, 2020\\
University of Heidelberg, Germany\\
*Efficient Event Generation with Normalizing Flows*

- (virtual) Talk at "Higgs and Effective Field Theories (HEFT) 2020, April 16, 2020\\
via Vidyo, hosted by the Universidad de Granada, Spain\\
*The One-Loop Renormalization Group Equations of the Higgs-Electroweak Chiral Lagrangian*

- (virtual) Theory Seminar at NIKHEF, April 9, 2020\\
NIKHEF Theory Group, Amsterdam, The Netherlands\\
*Numerical Integration and Event Generation with Normalizing Flows*

- Particle Physics Seminar at the University of Notre Dame, January 28, 2020\\
Physics Department, University of Notre Dame, Notre Dame, USA\\
*i-flow: Numerical Integration and Event Generation with Normalizing Flows*

- Talk at "Particle Physics in Computing Frontier", December 10, 2019\\
Institute for Basic Science, Daejeon, South Korea\\
*Event Generation with Normalizing Flows*

- Physics Seminar at the University at Buffalo, October 8, 2019\\
Physics Department, University at Buffalo, Buffalo, USA\\
*Improving Numerical Integration and Event Generation with Normalizing Flows*

- Talk at "In Search of New Physics Using SMEFT", October 2, 2019\\
Argonne National Laboratory, Chicago, USA\\
*Renormalization of the bosonic SMEFT operators*

- HET Brown Bag Seminar at the LCTP, September 25, 2019\\
Leinweber Center for Theoretical Physics, University of Michigan, Ann Arbor, USA\\
*Improving Numerical Integration and Event Generation with Normalizing Flows*

- Blackboard talk at "The Energy Frontier Beyond LHC Run 2", September 12, 2019\\
Aspen Center for Physics, Aspen, USA\\
*Numerical Integration and Event Generation with Normalizing Flows*

- Talk at "Multibosons at the Energy Frontier", July 26, 2019\\
Fermi National Accelerator Laboratory (Fermilab), Chicago, USA\\
*Effective Field Theories and Anomalous Gauge Couplings*

- Theory Seminar at MITP, May 7, 2019\\
Mainz Institute for Theoretical Physics, Johannes Gutenberg-Universität Mainz, Germany\\
*Higgs Effective Field Theories and their Renormalization*

- Theory Seminar at DESY, April 29, 2019\\
Deutsches Elektronen Synchrotron (DESY), Hamburg, Germany\\
*Higgs Effective Field Theories and their Renormalization*

- Talk at "Higgs Effective Field Theories (HEFT) 2019, April 17, 2019\\
Université catholique de Louvain, Belgium\\
*Master formula for one-loop renormalization of bosonic SMEFT operators*

- Theory Seminar at Argonne, March 19, 2019\\
Argonne National Laboratory, Chicago, USA\\
*Higgs Effective Field Theories and their Renormalization*

- Theory Seminar at Cornell University, March 6, 2019\\
Cornell University, Ithaca, USA\\
*Higgs Effective Field Theories and their Renormalization*

- Remote Talk at "Higgs HL/HE-LHC Autumn WG2 Meeting", October 22, 2018\\
CERN, Geneva, Switzerland\\
*&kappa; vs. nonlinear Effective Theory & Higgs couplings precision overview*

- Talk at "Chiral Dynamics 2018", September 17, 2018\\
Duke University, Durham, USA\\
*Complete One-Loop Renormalization of the Higgs-Electroweak Chiral Lagrangian*

- Talk at "Particles, Strings, and Cosmology (PASCOS) Symposium 2018", June 5, 2018\\
Case Western Reserve University, Cleveland, USA\\
*Current and Future Constraints on Higgs Couplings*

- Theory Seminar at ASC, May 2, 2018\\
Arnold-Sommerfeld Center for Theoretical Physics, Ludwig Maximilian University, Munich, Germany\\
*Current and future constraints on Higgs couplings in the nonlinear Effective Theory*

- Theory Seminar at IFIC, April 24, 2018\\
Instituto de Física Corpuscular, Valencia, Spain\\
*Current and future constraints on Higgs couplings in the nonlinear Effective Theory*

- Talk at "Higgs Effective Field Theories (HEFT) 2018, April 16, 2018\\
Johannes Gutenberg-Universität Mainz, Germany\\
*Complete One-Loop Renormalization of the Electroweak Chiral Lagrangian*

- Talk at "HL/HE LHC Meeting", April 5, 2018\\
Fermi National Accelerator Laboratory (Fermilab), Chicago, USA\\
*Current and future constraints on Higgs couplings in the nonlinear Effective Theory*

- Preisträgervortrag for the "Universe Cluster Ph.D. Award", January 18, 2018\\
Excellence Cluster "Universe", Munich, Germany\\
*Higgs Effective Field Theories &mdash; Systematics & Application*

- Talk at "LHC Pheno", December 19, 2017\\
Instituto de Física Corpuscular, Valencia, Spain\\
*Signals of electroweak baryogenesis and the role of Higgs self-couplings*

- Talk at "Higgs Couplings 2017", November 8, 2017\\
University of Heidelberg, Germany\\
*A Bayesian Fit to Higgs Data Using HEPfit and the Electroweak Chiral Lagrangian*

- Theory Seminar at the ITP, Heidelberg University, October 24, 2017\\
University of Heidelberg, Germany\\
*Effective Field Theories in Higgs Physics*

- Theory Seminar at Argonne, July 11, 2017\\
Argonne National Laboratory, Chicago, USA\\
*Implications of Non-Decoupling UV-Physics*

- Talk at "Higgs Effective Field Theories (HEFT) 2017", May 23, 2017\\
Lumley Castle, Durham, United Kingdom\\
*Implications of Non-Decoupling UV-Physics*

- Talk at "Understanding the LHC", February 12, 2017\\
Physikzentrum Bad Honnef, Germany\\
*Heavy Resonances in the Electroweak Chiral Lagrangian*

- Theory Seminar at Fermilab, November 15, 2016\\
Fermi National Accelerator Laboratory (Fermilab), Chicago, USA\\
*Higgs Effective Field Theories &mdash; Systematics & Applications*

- Talk at "Higgs Couplings 2016", November 10, 2016\\
Stanford Linear ACcelerator (SLAC), Stanford, USA\\
*Choosing the Appropriate EFT for Higgs Analyses*

- Talk at "Young Scientist Workshop", June 8, 2016\\
Castle Ringberg, Tegernsee, Germany\\
*Higgs Effective Field Theories*

- Theory Seminar at IFIC, May 19, 2016\\
Instituto de Física Corpuscular, Valencia, Spain\\
*The electroweak chiral Lagrangian with a light Higgs &mdash; systematics & application*

- Talk at "Frühjahrstagung der Deutschen Physikalischen Gesellschaft (DPG)", March 1, 2016\\
University of Hamburg, Germany\\
*QFT Justification of the &kappa;-framework using the electroweak chiral Lagrangian*

- Talk at "IMPRS Particle Physics", December 11, 2015\\
Max Planck Institute for Physics, Munich, Germany\\
*Fitting LHC Data using Effective Field Theories*

- Theory Seminar at UCSD, November 17, 2015\\
University of California, San Diego, USA\\
*The electroweak chiral Lagrangian with a light Higgs &mdash; systematics & application*

- Theory Seminar at PI, November 13, 2015\\
Perimeter Institute, Waterloo, Canada\\
*The electroweak chiral Lagrangian with a light Higgs &mdash; systematics & application*

- Theory Seminar at MITP, November 10, 2015\\
University of Michigan, Ann Arbor, USA\\
*The electroweak chiral Lagrangian with a light Higgs &mdash; systematics & application*

- Talk at "Higgs Effective Field Theories (HEFT) 2015", November 4-6, 2015\\
University of Chicago, USA\\
*Justifying the &kappa;-framework with the non-linear EFT*

- Talk at "Herbstschule für Hochenergiephysik", September 8-18, 2015\\
Maria Laach, Germany\\
*Die Nutzung effektiver Feldtheorien zur Charakterisierung des "Higgs"-Teilchens*

- Talk at "Frühjahrstagung der Deutschen Physikalischen Gesellschaft (DPG)", March 9-13, 2015\\
Wuppertal, Germany\\
*Higgs beyond the Standard Model - an EFT approach*

- Talk at "Strong Interactions in the LHC Era", November 12-14, 2014\\
Physikzentrum Bad Honnef, Germany\\
*Higgs beyond the Standard Model - an EFT approach*

- Talk at "New Physics Within and Beyond the Standard Model", September 11, 2014\\
Oberwölz, Austria\\
*Higgs beyond the Standard Model - an EFT approach*

- Talk at "Young Scientist Workshop", July 17, 2014\\
Castle Ringberg, Tegernsee, Germany\\
*Higgs beyond the Standard Model, an Effective Field Theory approach*

- Talk at "Frühjahrstagung der Deutschen Physikalischen Gesellschaft (DPG)", March 24-28, 2014\\
JGU Mainz, Germany\\
*Higgs beyond the Standard Model - an EFT approach*

- Talk at "Physics beyond the Higgs (Winter School)", March 1-8, 2014\\
Schladming, Austria\\
*Higgs beyond the Standard Model - an EFT approach*

- Talk at "Universe Cluster Science Day", September 26, 2013\\
Garching, Germany\\
*An effective field theory for electroweak symmetry breaking including a light Higgs*


