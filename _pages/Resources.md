---
layout: page
title: Resources
permalink: /resources/
---


#### Higgs Effective Field Theories (HEFT) - Workshop Series
- [2013 - CERN](https://indico.cern.ch/event/258552/)
- [2014 - IFT Madrid](https://workshops.ift.uam-csic.es/HEFT2014)
- [2015 - University of Chicago](http://heft2015.uchicago.edu/)
- [2016 - NBI Copenhagen](https://indico.nbi.ku.dk/event/855/)
- [2017 - IPPP Durham](https://conference.ippp.dur.ac.uk/event/590/)
- [2018 - JGU Mainz](https://indico.mitp.uni-mainz.de/event/160/overview)
- [2019 - UC Louvain](https://agenda.irmp.ucl.ac.be/event/3283/overview)
- [2020 - via Vidyo, hosted by Universidad de Granada](https://indico.cern.ch/event/855352/overview)
- [2021 - USTC, Hefei, China](https://indico.ihep.ac.cn/event/13632/)
- [2022 - Universidad de Granada](https://indico.cern.ch/event/1161919/)
- [2023 - University of Manchester](https://indico.cern.ch/event/1234517/)
- [2024 - Bologna](https://indico.cern.ch/event/1339526/)

#### Machine Learning for Jet Physics (ML4Jets) - Workshop Series
- [2017 - Berkeley](https://indico.physics.lbl.gov/event/546/)
- [2018 - Fermilab](https://indico.cern.ch/event/745718/)
- [2020 - NYU](https://indico.cern.ch/event/809820/)
- [2021 - Heidelberg](https://indico.cern.ch/event/980214/)
- [2022 - Rutgers University](https://indico.cern.ch/event/1159913/)
- [2023 - Hamburg University](https://indico.cern.ch/event/1253794/)
- [2024 - Paris](https://indico.cern.ch/event/1386125/)

#### Experiments
- [ATLAS](http://atlas.ch/) ([public results](https://twiki.cern.ch/twiki/bin/view/AtlasPublic))
- [CMS](http://cms.web.cern.ch/cms/index.html) ([public results](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResults))

#### Useful links
- [HEPML Living Review](https://iml-wg.github.io/HEPML-LivingReview/)
- [good coding practices for research](https://goodresearch.dev/)
- I cannot always rely on my brain, so I have a second brain powered by [Obsidian](https://obsidian.md/).

