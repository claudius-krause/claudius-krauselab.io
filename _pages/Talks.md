---
layout: page
title: Talks
permalink: /talks/
---

Here you can find the [poster](/assets/Poster_EuCAIFCon_Flows4Calo.pdf) I presented at [EuCAIFCon 2024](https://indico.nikhef.nl/event/4875/). 

#### Recent Talks
<!-- - Title  ([pdf](/assets/Talks/name.pdf))-->

Here you can find a selected list of talks I recently gave. The full list is available on the [CV](/cv/) page.

- "CaloChallenge 2022 &mdash; Final Evaluation and Lessons Learned", \\
Plenary Talk at the ML4Jets Conference, November 6, 2024\\
LPNHE, Sorbonne University, Paris, France\\
([pdf](/assets/Talks/CaloChallenge.FinalEval.C.Krause.pdf))

- "The Fast Calorimeter Simulation Challenge 2022",\\
Joint virtual meeting of the HSF detector simulation and ML4Sim working groups,\\
([pdf](/assets/Talks/CaloChallenge.C.Krause.pdf))

- "CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows", \\
Fermilab Theory Seminar, Fermilab, USA,\\
([pdf](/assets/Talks/C.Krause.CaloFlow.pdf))

- "Event Generation with Normalizing Flows",\\
[Particle Physics in Computing Frontier](https://indico.ibs.re.kr/event/325/overview), Institute for Basic Science, Daejeon, South Korea,\\
([pdf](/assets/Talks/NormFlow.C.Krause.pdf))

- "Improving Numerical Integration and Event Generation with Normalizing Flows",\\
HET Brown Bag Seminar, Leinweber Center for Theoretical Physics, University of Michigan, Ann Arbor, USA,\\
and \\
Physics Seminar, University at Buffalo, Buffalo, USA, 
([pdf](/assets/Talks/C.Krause.Norm.Flow.pdf))

- "Effective Field Theories and Anomalous Gauge Couplings",\\
[Multibosons At The Energy Frontier Workshop](https://indico.cern.ch/event/823181/), Fermilab, ([pdf](/assets/Talks/LPC_Multibosons_Krause.C.pdf)) 

- "Higgs Effective Field Theories and their Renormalization",\\
Theory Seminar, multiple locations[^1], ([pdf](/assets/Talks/Th.Palaver.C.Krause.pdf))

- "Master formula for one-loop renormalization of bosonic SMEFT operators",\\
[HEFT 2019 Workshop](https://agenda.irmp.ucl.ac.be/event/3283/overview), UC Louvain, ([pdf](/assets/Talks/C.Krause.HEFT19.pdf))

- "Complete One-Loop Renormalization of the Higgs-Electroweak Chiral Lagrangian",\\
[Chiral Dynamics 2018 Workshop](https://www.cd18.org/), Duke University, Durham, NC, ([pdf](/assets/Talks/C.Krause.CD18.pdf)) 

- "Current and Future Constraints on Higgs Couplings",\\
[PaSCos Symposium](https://artsci.case.edu/pascos2018/), Case Western University, Cleveland, OH, ([pdf](/assets/Talks/C.Krause.PASCOS.pdf))

<br><br><br>


[^1]: Argonne National Laboratory, Cornell University, DESY Hamburg, JGU Mainz