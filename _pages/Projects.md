---
layout: page
title: Projects
permalink: /projects/
---

### Projects that have repositories with code:

#### Numerical Integration with Normalizing Flows: i-flow

We use normalizing flows to improve importance sampling in numerical integration. [Here](https://gitlab.com/i-flow/i-flow), we have our code *i-flow* that implements this idea and [here](https://arxiv.org/abs/2001.05486) we describe it in detail. 

#### Simulating Calorimeter Showers with Normalizing Flows: CaloFlow

We use normalizing flows as deep generative model to simulate calorimeter showers of positrons, photons, and pions. Our code *CaloFlow* is available on [gitlab](https://gitlab.com/claudius-krause/caloflow). *CaloFlow v1*, which implements sampling via a MAF is described [here](https://arxiv.org/abs/2106.05285); *CaloFlow v2*, which implements sampling via an IAF and is trained with probability density distillation is described [here](https://arxiv.org/abs/2110.11377). This project is the first application of normalizing flows to detector simulation and the first deep generative model that passes the classifier test of distinguishing <q>real</q> (based on GEANT4) from <q>fake</q> (based on the surrogate).

#### Enhancing anomaly detection in bump hunts: CATHODE

In *Classifying Anomalies THrough Outer Density Estimation (CATHODE)*, we use normalizing flows to enhance anomaly searches based on bump hunts. Using the [LHC Olympics 2020 R&amp;D dataset](https://lhco2020.github.io/homepage/), we show that CATHODE can enhance potential signals up to the theoretical maxium, set by a classifier trained to distinguish perfectly modeled background and data. The publice repository of this project is [here](https://github.com/HEPML-AnomalyDetection/CATHODE), the paper describing the method is available [here](https://arxiv.org/abs/2109.00546).

#### Study of Electroweak Symmetry Non-Restoration

In [this paper](https://arxiv.org/abs/2104.00638) we compute the effective potential of an extended Two-Higgs-Doublet model and study electroweak symmetry non-restoration at high temperatures. The code for the numerical evaluation can be found [here](https://gitlab.com/claudius-krause/ew_nr).