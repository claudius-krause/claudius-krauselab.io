---
layout: page
title: Research
permalink: /research/
---

I am a theoretical particle physicist and interested in physics beyond the Standard Model (SM). In my research, I want to answer fundamental questions about Nature around us. At the moment, I am interested in

- Machine Learning Techniques:\\
_How can we use advanced Neural Networks to improve data analysis and simulation in Particle Physics?_

- Effective Field Theories and their application to Higgs physics and the Standard Model:\\
_How can we analyze the current experimental data as model-independently as possible and at the same time use a format that will still be usefull in 10 years from now?_

- The Phenomenology of the Higgs particle:\\
_Is it really the Higgs particle, as predicted by the Standard Model?_\\
_And if it is not the Standard Model Higgs, what else is it?_\\
_Can we explain the mass hierarchies or matter-antimatter asymmetry with it?_

<br>

Below are some details on certain topics of interest that I have worked on. A complete list of publications can be found [on inspirehep](http://inspirehep.net/search?p=exactauthor%3AC.Krause.1&sf=earliestdate) and on the [CV page](/cv/). Some new, cutting-edge and confidencial research I'm doing can be found [here](https://www.rickrolled.com).

### Utilizing normalizing flows for particle physics simulations and analyses

A normalizing flow is a type of neural network that learns a coordinate transformation with analytical inverse and trackable Jacobian. This has various applications for particle physics:


- Simulating particle collisions with Monte Carlo event generators requires sampling random numbers (events) according to a given distribution (the differential cross section). In [this project](https://arxiv.org/abs/2001.10028) [(published here)](https://doi.org/10.1103/PhysRevD.101.076002), we used normalizing flows to learn the distribution of events in phase space and improve the unweighting efficiency of Vector plus jets production by a factor of three. We published our code for numerical integration with normalizing flows --- *i-flow* --- separately. The paper is available on [arXiv](https://arxiv.org/abs/2001.05486), but is also published in [ML:ST](https://doi.org/10.1088/2632-2153/abab62). The code is available on [gitlab](https://gitlab.com/i-flow/i-flow). 

- Simulating the interaction of particles with the detector is very time consuming using the GEANT-4 software. In [this project](https://gitlab.com/claudius-krause/caloflow), we introduce [CaloFlow v1](https://arxiv.org/abs/2106.05285), a deep generative model based on normalizing flows that generates calorimeter shower of positrons, photons, and pions in an ATLAS-inspired ECal. This MAF-based flow already gives samples of high fidelity. However, this approach is still not fast enough when compared to other deep generative models such as GANs. In [CaloFlow v2](https://arxiv.org/abs/2110.11377), we train an IAF-based flow based on a method called probability density distillation. As a result, we have a generative model that produces calorimeter showers as fast as a GAN, but with a quality that gives a neural binary classifier a hard time distinguishing true from fake showers. 

- In [CATHODE](https://github.com/HEPML-AnomalyDetection/CATHODE) we use normalizing flows to enhance bump hunts. These model-agnostic anomaly searches look for overdensities in distributions that come from new physics. Our method learns the conditional density based on the events in the sideband region. We then sample artificial events in the signal region that follow the background-only distribution. In the last step, we train a binary classifier to distinguish these events from actual events in the signal region. If there is an overdensity of events in the signal region, events that are classified as data-like will be signal-enriched. By working with the [LHC-Olympics dataset](https://github.com/LHCO2020/homepage), we were able to reach signal enhancements of 14, approaching the maximal theoretically possible score (set be a classifier trained on perfectly modeled background). The method is described in more detail [here](https://arxiv.org/abs/2109.00546).


### Development and Application of the non-linear Higgs electroweak Chiral Lagrangian

- We defined a basis of NLO operators based on a power counting that is consistent with an expansion in loops. The resulting EFT is renormalizable order by order in the effective expansion.\\
This project was my Master's Thesis and is published on [arXiv](https://arxiv.org/abs/1307.5017) and in  [Nucl.Phys. B](https://doi.org/10.1016/j.nuclphysb.2014.01.018).

- We reformulated the power counting using the concept of Chiral Dimensions, which are a generalization of the momentum counting of Chiral Perturbation Theory.\\
We published our results on [arXiv](https://arxiv.org/abs/1312.5624) and in [Phys.Lett. B](https://doi.org/10.1016/j.physletb.2014.02.015).

- We studied how experimental results from the LHC constrain the effective couplings.\\
First, we showed that current results only constrain a few of the couplings and that the resulting effective Lagrangian gives a Quantum-Field-Theoretical justification of the &kappa;-framework. The latter is used by the experimental collaborations to report the results of Higgs measurements. Our note is available on [arXiv](https://arxiv.org/abs/1504.01707) and published in [Phys.Lett. B](https://doi.org/10.1016/j.physletb.2015.09.027).\\
Data of LHC Run-1, available in fall 2015, constrained the effective couplings to be within 10%-20% of their SM expectations. This fit is available on [arXiv](https://arxiv.org/abs/1511.00988) and published in [Eur.Phys.J.C](https://doi.org/10.1140/epjc/s10052-016-4086-9).\\
Once data from LHC Run-2 is included, the uncertainties decrease by roughly a factor of 2. In our publication, available on [arXiv](https://arxiv.org/abs/1803.00939) and published in [JHEP](https://doi.org/10.1007/JHEP07(2018)048), we studied in detail the prior dependence of our Bayesian fit and extrapolated how the uncertainties will scale at various future colliders scenarios that are discussed in the community right now. 

Most of these projects were part of my Doctoral Thesis, *"Higgs Effective Field Theories &mdash; Systematics & Applications"*, which can be found on [arXiv](https://arxiv.org/abs/1610.08537) and in the [University Library](https://edoc.ub.uni-muenchen.de/19873/).

### Derivation and application of a Masterformula for one-loop divergences

Based on the Background-Field Method and Super-Heat-Kernel Techniques, we derived a Masterformula to find all one-loop divergences of a given Lagrangian.

- We first applied the Masterformula to the non-linear Higgs electroweak Chiral Lagrangian. We showed that all divergent structures can be expressed through the basis that we had defined previously. Our results can be found on [arXiv](https://arxiv.org/abs/1710.06412) and in [Nucl.Phys. B](https://doi.org/10.1016/j.nuclphysb.2018.01.009). In a [follow-up project](https://arxiv.org/abs/2004.11348), we computed all renormalization group equations (RGEs) of the electroweak Chiral Lagrangian. The RGEs are published in [Phys.Rev.D](https://doi.org/10.1103/PhysRevD.104.076005).

- In a second project, we generalized the Masterformula to also cover single bosonic dimension 6 vertices in the Lagrangian. We used this Masterformula to confirm the renormalization group equations (RGEs) of the bosonic dimension 6 operators of the SMEFT. Our preprint is available on [arXiv](https://arxiv.org/abs/1904.07840).

- Right now (still is work in progress), we generalize the Masterformula even further, such that the full RGEs of all dimension 6 SMEFT operators can be computed. 

### BSM physics affecting the electroweak phase transition

- If the physics beyond the SM is heavier than the SM particles, we can use an EFT approach to study its impact on the electroweak phase transition. Previous studies showed that in presence of dimension 6 operators in the Higgs potential, the scale of new physics has to be rather low in order to have a sufficiently strong phase transition. We concluded that dimension 8 operators might therefore also be important and studied their effect. We also looked at concrete model realizations that would create such a pattern of effective operators and studied how we could find them at collider experiments. Our paper is available on [arXiv](https://arxiv.org/abs/1802.02168) and published in [JHEP](https://doi.org/10.1007/JHEP07(2018)062).

- An interesting approach to baryogenesis is given by electroweak symmetry non-restoration. In these thermal histories, the electroweak symmetry remains broken up to high temperatures (well above the electroweak scale). In [a recent project](https://arxiv.org/abs/2104.00638), we proposed a new approach for electroweak symmetry non-restoration via an inert Higgs sector. In this numerical study, utilizing renormalization group improvements and thermal resummation, we showed non-restoration for two benchmark scenarios up to tens of TeV in temperature. Further, we compute the suppression of the sphaleron washout factors of these setups. Our work is published in [Phys.Rev.D](https://doi.org/10.1103/PhysRevD.104.055016).


