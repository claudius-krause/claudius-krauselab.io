---
layout: page
title: Ubuntu 20.04 on a Dell XPS 15 9500
permalink: /journey/
---

### My journey to set up Ubuntu 20.04 on a Dell XPS 15 9500, step by step.

My Dell XPS 15 9500 came with Windows 10 installed and I wanted to set it up in dual boot with Ubuntu. For the first steps, I followed the guides I found [here](https://medium.com/@asad.manji/my-journey-installing-ubuntu-20-04-on-the-dell-xps-15-9500-2020-8ac8560373d1) and [here](https://blog.usejournal.com/my-journey-installing-ubuntu-18-04-on-the-dell-xps-15-7590-2019-756f738a6447).

- Step 1 is to boot into Win 10 and complete the setup. Then, I updated Windows and the Dell software.

- For future reference, I wrote down the Windows activation key and the serial number. This can be done in the shell prompt (cmd) via `wmic path SoftwareLicensingService get OA3xOriginalProductKey` and `wmic bios get SerialNumber`.

- In Windows 10, go to "Create and Format Hard Disk Partitions" and shrink current volume to 256000MB (250GB). Then go to cmd shell and type `bcdedit /set {current} safeboot minimal`. Restart and enter BIOS, change SATA from RAID to AHCI. Boot back up and type `bcdedit /deletevalue {current} safeboot`. Finally, go to settings and disable device encryption (bitlocker).

- Now we can Download Ubunut 20.04 .iso image from [here](https://ubuntu.com/download/desktop), `ubuntu-20.04.2.0-desktop-amd64.iso`

- Then, we grab a USB and make it bootable with the image like described [here](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview)



This site is currently under construction. Please be patient. 